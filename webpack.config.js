const webpack = require("webpack");
const path = require("path");
const pathToDist = __dirname + "/src/main/resources/static/js";

module.exports = {

    entry: {
        "bundle": "./ts/main.tsx",
    },

    devtool: "source-map",

    output: {
        filename: `bundle.js`,
        path: pathToDist
    },

    resolve: {
        modules: [
            "./",
            "node_modules"
        ],
        extensions: [".js", ".css", ".ts", ".tsx", ".scss"]
    },

    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },

            {
                test: /\.tsx?$/,
                loader: "babel-loader" +
                "?presets[]=es2015" +
                "!ts-loader"
            },

            {test: /\.scss$/, use: ["style-loader", "css-loader", "sass-loader"]},
            {test: /\.css$/, use: ["style-loader", "css-loader"]},
            {test: /\.(png|jpg)$/, loader: `file-loader?name=images/[name].[ext]`},
            {test: /\.(woff2?|svg)$/, loader: `file-loader?name=fonts/[name].[ext]`},
            {test: /\.(ttf|eot)$/, loader: `file-loader?name=fonts/[name].[ext]`}
        ]
    },

    plugins: [
    ]
};