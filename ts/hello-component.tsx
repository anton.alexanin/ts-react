import * as React from "react";
import axios from "axios";
import {observer} from "mobx-react";
import {action, computed, observable} from "mobx";
import {Park} from "./park";


import "./../style/main";

@observer
export class HelloComponent extends React.Component<{}, {}> {

    parkList: Park[] = observable([]);

    async loadData(): Promise<Park[]> {
        return axios.get("/getRandomList")
            .then(response => {
                return response.data;
            });
    }

    @action setParkList() {
        this.loadData().then(value => value.forEach(park => this.parkList.push(park)));
    }

    @computed get parkCount(): number {
        return this.parkList.length;
    }

    render() {

        let table: JSX.Element[] = [];

        this.parkList.forEach((value) => {
            table.push(<ParkRow park={value} key={`${value.id}${value.name}${value.createTime}`}/>);
        });

        return (
            <div>
                <table className="park-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Create Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    {table}
                    </tbody>
                </table>

                <button onClick={() => this.setParkList()}>
                    Get Random Park
                </button>

            </div>
        );
    }
}

interface ParkRowProps {
    park: Park;
}

class ParkRow extends React.Component<ParkRowProps, {}> {

    render() {
        return (
            <tr>
                <td>{this.props.park.id}</td>
                <td>{this.props.park.name}</td>
                <td>{this.props.park.createTime}</td>
            </tr>
        );
    }

}