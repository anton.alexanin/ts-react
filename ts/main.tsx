import * as React from "react";
import * as ReactDOM from "react-dom";
import {HelloComponent} from "./hello-component";
import "babel-polyfill";

console.log("Hi!");

ReactDOM.render(
    <HelloComponent/>,
    document.getElementById("app")
);
