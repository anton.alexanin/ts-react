export interface Park {
    id: number;
    name: String;
    createTime: number;
}