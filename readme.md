# Compiling

For Windows install [Node](https://nodejs.org/)

For Ubuntu use [NVM](https://github.com/creationix/nvm)

## Dependencies

You can use both `yarn` or `npm`.

`npm` is straight forward:
- `npm i` to install dependencies
- `npm run watch` to run `webpack` in watch mode

For `yarn` there are scripts presented:
- if there is no `yarn` in the system use `npm run full-install`.
It will globally install yarn and download dependencies
- if `yarn` is presented in the system, you can use `yarn install`
or `npm run dependencies-install` to install dependencies
- `yarn run watch` to run `webpack` in watch mode

# Running

- `yarn run webpack` to compile frontend
- `yarn run run` to run spring application