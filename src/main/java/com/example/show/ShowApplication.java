package com.example.show;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@SpringBootApplication
public class ShowApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShowApplication.class, args);
    }
}

@RestController
class MainController {

    @RequestMapping("/getRandomList")
    public static List<Park> getRandomList() {
        List<Park> dummyList = new ArrayList<>();

        int count = ThreadLocalRandom.current().nextInt(1, 5);

        for (int i = 0; i <= count; i++) {
            Park park = new Park();
            park.setId(i);
            park.setName(UUID.randomUUID().toString());
            park.setCreateTime(Date.from(Instant.now()));

            dummyList.add(park);
        }

        return dummyList;
    }

}

@Data
class Park {
    private int id;
    private String name;
    private Date createTime;
}